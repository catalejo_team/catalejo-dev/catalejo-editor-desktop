# catalejo-editor-desktop


## Iniciar

Clona este repositorio

```bash
git clone git@gitlab.com:catalejo_team/catalejo-dev/catalejo-editor-desktop.git
```

Descargar e instalar los submódulos

```bash
git submodule update --init --recursive
```

## Actualizar proyecto y submódulos

```bash
git pull
git submodule update --recursive
```
